Rails.application.routes.draw do
  resources :costs
  resources :incomes
  resources :categories
  root 'categories#index'
end
