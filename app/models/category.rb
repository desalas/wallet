class Category < ActiveRecord::Base
  has_many :costs, :dependent => :destroy
  has_many :incomes, :dependent => :destroy
end
