class CostsController < ApplicationController
  before_action :set_cost, only: [:show, :edit, :update, :destroy]

  # GET /costs
  # GET /costs.json
  def index
    @costs = Cost.all
  end

  # GET /costs/1
  # GET /costs/1.json
  def show
  end

  # GET /costs/new
  def new
    @cost = Cost.new
  end

  # GET /costs/1/edit
  def edit
  end

  # POST /costs
  # POST /costs.json
  def create
    @cost = Cost.new(cost_params)

    respond_to do |format|
      if @cost.save
        flash[:success] = 'Cost was successfully created.'
        format.html { redirect_to @cost }
        format.json { render :show, status: :created, location: @cost }
      else
        flash[:danger] = 'There was a problem creating the Cost.'
        format.html { render :new }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /costs/1
  # PATCH/PUT /costs/1.json
  def update
    respond_to do |format|
      if @cost.update(cost_params)
        flash[:success] = 'Cost was successfully updated.'
        format.html { redirect_to @cost }
        format.json { render :show, status: :ok, location: @cost }
      else
        flash[:danger] = 'There was a problem updating the Cost.'
        format.html { render :edit }
        format.json { render json: @cost.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /costs/1
  # DELETE /costs/1.json
  def destroy
    @cost.destroy
    respond_to do |format|
      flash[:success] = 'Cost was successfully destroyed.'
      format.html { redirect_to costs_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_cost
    @cost = Cost.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def cost_params
    params.require(:cost).permit(:amount, :category_id)
  end
end
