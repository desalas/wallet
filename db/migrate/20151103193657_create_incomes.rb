class CreateIncomes < ActiveRecord::Migration
  def change
    create_table :incomes do |t|
      t.integer :amount
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
